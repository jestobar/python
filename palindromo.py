def palindrome2(word):
    reverse_letters= word[::-1]
    if reverse_letters == word:
        return True
    return False

def palindrome(word):
    reverse_letters= []
    for letter in word:
        reverse_letters.insert(0,letter)

    reversed_word=''.join(reverse_letters)
    if reversed_word == word:
        return True
    return False



def run():
    word = input('Escribe una palabra:')
    result =  palindrome2(word)

    if result is True:
        print('la palabra {} si es palindromo'.format(word))
    else:
        print('la palabra {} NO es palindromo'.format(word))


if __name__ == "__main__":
    run()