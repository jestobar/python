from flask import Flask,render_template

app= Flask(__name__)


@app.route('/')
def hellow_word():
    return render_template('contact_book.html')


if __name__ == "__main__":
    app.run()