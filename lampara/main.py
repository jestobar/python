from lamp import Lamp # mayusculas son las clases
#import lamp

def run():
    lamp=Lamp(_is_turned_on = False)
    while True:
        command= str(input('''
        Que deseas hacer?
        [p]render
        [a]pagar
        [s]salir

        '''))
        if command == 'p':
            lamp.turn_on()
        elif command == 'a':
            lamp.turn_off()
        else:
            break

if __name__ == "__main__":
    run()