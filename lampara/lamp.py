class Lamp:
    _Lamps=['''
           .
     .    |    ,
      \   '   /
       ` ,-. '
    --- (   ) ---
         \ /
        _|=|_
       |_____|
    ''',
    '''
         ,-.
        (   )
         \ /
        _|=|_
       |_____|
    
    ''']
    def __init__(self,_is_turned_on):
        self._is_turned_on = _is_turned_on
    def turn_on(self):
        self._is_turned_on=True
        self._display_Image()
    def turn_off(self):
        self._is_turned_on= False
        self._display_Image()
    def _display_Image(self):
        if self._is_turned_on:
            print(self._Lamps[0])
        else:
            print(self._Lamps[1])
