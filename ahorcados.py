import random

IMAGES = ['''

    +---+
    |   |
        |
        |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
        |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
    |   |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|   |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
        |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
        |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
   /    |
        =========''', '''

    +---+
    |   |
    O   |
   /|\  |
    |   |
   / \  |
        =========''', '''
''']

WORDS=[
    'lavadora',
    'secadora',
    'sofa',
    'gobierno',
    'diputado',
    'democracia',
    'computadora',
    'teclado'
]
def random_word():
    idx = random.randint(0,len(WORDS)-1)
    return WORDS[idx]

def display_board(hidden_word, tries):
    print(IMAGES[tries])
    print('')
    print(hidden_word)
    print('--------*---------- --------*---------- --------*---------- --------*----------')


def run():
    word = random_word()
    hidden_word = ['-'] * len(word)
    tries = 0
    while True:
        display_board(hidden_word,tries)
        current_letter = str(input('Escoge una letra:  '))

        letter_idexes= []
        for idx in range(len(word)):
            if word[idx] == current_letter:
                letter_idexes.append(idx)
        if len(letter_idexes) == 0:
            tries += 1
            if tries == 7:
                display_board(hidden_word,tries)
                print('Perdiste la palabra correcta era {}'.format(word))
                break
        else:
            for idx in letter_idexes:
                hidden_word[idx] =  current_letter
            letter_idexes = []
        try:
             hidden_word.index('-')
         
        except ValueError:
            print('Ganaste. La palabra era {}'.format(word))
            break



if __name__ == "__main__":
    print('Bienvenidos a Ahorcados')
    run()