def calcula_temp(tempera):
    suma_temp = 0
    for temp in tempera:
        suma_temp += temp

    return suma_temp / len (tempera)
 

if __name__ == "__main__":
    temp= [21,24,24,22,20,23,24]
    result =  calcula_temp(temp)
    print ('La temperatura promedio es {}'.format(result))
