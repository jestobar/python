
def binary_search(numbers,number_to_Find,low,high):
    if low>high:
        return False
    mid = int((low+high)/2)

    if numbers[mid] == number_to_Find:
        return True
    elif numbers[mid]> number_to_Find:
        return binary_search(numbers,number_to_Find,low,mid - 1)
    else:
        return binary_search(numbers,number_to_Find,mid+1,high)

if __name__ == "__main__":
     numbers = [39,1, 60,3, 4, 5,24, 6, 9, 10, 11, 25,64,27, 28, 34, 36,47,31, 49, 51]
     number_to_Find = int(input('Ingresa un numero : '))
     numbers.sort()
     print(numbers) 

     lenlist = (len(numbers) - 1)
     

     result=  binary_search(numbers,number_to_Find,0,lenlist)

     if result is True:
         print('El numero si esta en la lista')
     else:
         print('EL numero no esta en la lista')