def verificaletra(palabra):
    seen_letters = {}
    for  idx , letter in enumerate(palabra):
        if letter not in seen_letters:
            seen_letters[letter]=(idx,1)
        else:
            seen_letters[letter]= (seen_letters[letter][0],seen_letters[letter][1]+1)
    final_letters = []
    for key,value in seen_letters.items():
        if value[1]== 1:
            final_letters.append((key,value[0]))
    not_repite_letters   = sorted(final_letters,key=lambda value: value[1])  
    if not_repite_letters:
        return not_repite_letters[0][0]
    else:
        return '_'

        
def run():
     palabra = str(input('Ingrese la palabra '))
     result = verificaletra(palabra)
     if result == '_':
        print('Todos los caracteres se repiten')
     else:
        print('El primer caracter no respetido es:{}'.format(result))


if __name__ == "__main__":
    run()